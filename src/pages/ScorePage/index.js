const ScorePage = ({score, num_question, playAgain,newCategory}) => {
    return (
        <div>
            <div>
                your score is {score} / {num_question} correct answer
            </div>
            <div>
                <button onClick={playAgain}>play again</button>
                <button onClick={newCategory}>new category</button>
            </div>
            <div>
                <p>other category </p>

            </div>
        </div>
    )
}
export default ScorePage;