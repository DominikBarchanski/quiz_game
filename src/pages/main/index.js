import React, {useState} from 'react';
import QuestionLoader from "../../components/Question_loader";
//TODO make more test with question and with all logic of app
const MainPage =props =>{
    const categoryList=[];
    const [category,setCategory]=useState();
    const [test,setTest]=useState(false);
    Object.keys(props.question).forEach(function(key){

        categoryList.push(props.question[key]);
    })

    const handleCategoryBack=(truFalse)=>{
        setTest(truFalse)
    }

    const CategoryButton =()=>{
         return categoryList.map((categoryLists, idd) =>
        <button  className={["category_choice-button",categoryLists[0].name ].join(" ")}  onClick={()=>{setCategory(idd+1);  setTest(true)} } id={idd+1} key={idd+1}> {categoryLists[0].name }</button>
    );
    }

    return(
        <div className={"button__wrapper"}>
            <div >
                {test? <QuestionLoader question_list={categoryList[category-1]} setCategory={handleCategoryBack} /> :CategoryButton()}
            </div>
        </div>
    )

}
export default MainPage;