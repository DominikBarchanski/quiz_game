import './styles/themes/default/theme.scss';
import MainPage from "./pages/main";
import questions from './questions/questions.json'
function App() {
  return (
    <div>
    <MainPage question={questions.category}/>
    </div>
  );
}

export default App;
