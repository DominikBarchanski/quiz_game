
import {Correctansw} from './Correctansw';
import {Box} from './Box';

const QuestionBoxDnd = ({props, checkAnswer}) => {

    const temp_answer = props.answers;

    const handleAnswerBox = () => {
        return temp_answer.map((question, index) =>
            <Box name={question} key={index} id={index}/>

        )
    }


    return (<div>
        <div>
            <Correctansw  whole_question={props.question}/>
        </div>
        {handleAnswerBox()}
        <button onClick={checkAnswer} id="show-answer"> Sprawdź </button>

    </div>);


}
export default QuestionBoxDnd;