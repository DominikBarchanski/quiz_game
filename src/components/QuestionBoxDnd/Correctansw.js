import { useDrop } from 'react-dnd';
import { ItemTypes } from './ItemTypes';

export const Correctansw = () => {
    const [{ canDrop, isOver }, drop] = useDrop(() => ({
        accept: ItemTypes.BOX,
        drop: () => ({ name: 'Correctansw' }),
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    }));
    const isActive = canDrop && isOver;
    if (isActive) {

    }
    else if (canDrop) {

    }
//TODO make whole question build with gap
    return (<div ref={drop} role={'Dustbin'} id={"answer"} >

        {isActive ? 'Release to drop' : 'Drop answer here'}
    </div>);
};