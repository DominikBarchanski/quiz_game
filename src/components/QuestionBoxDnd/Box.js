import {useDrag} from 'react-dnd';
import { ItemTypes } from './ItemTypes';

export const Box = function Box({name, id}) {

    const [{isDragging},drag]=useDrag(()=>({
        type:ItemTypes.BOX,
        item:{name},
        end: (item,monitor)=> {
            const dropResult = monitor.getDropResult();
            if (item && dropResult) {
                document.getElementById("answer").innerHTML=item.name

            }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
            handlerId: monitor.getHandlerId(),
        }),
    }));

    return(<div className="dnd_item" ref={drag} role="Box" data-testid={`box-${name}`}  id={id}>{name}</div>)

};