import React, {useState} from 'react';
import QuestionBox from "../QuestionBox";

const QuestionLoader = ({question_list, setCategory}) => {
    const numberOfQuestion = 5;
    const usedQuestion = [];
    let quizQuestion = [];

    const [play,setPlay]=useState(false);
    const name = question_list[0].name;

    const Quest_temp = question_list.slice(1, question_list.length)

    const randQuestion = (min, max) => {

        if (usedQuestion.length < Quest_temp.length) {
            let counter = 0;
            do {
                let rand = Math.floor(Math.random() * (max - min)) + min;
                let check = true;
                for (let i = 0; i < usedQuestion.length; i++) {
                    if (usedQuestion[i] === rand) {
                        check = false;
                        break;
                    }
                }
                if (check) {
                    usedQuestion.push(rand)
                    if (counter >= usedQuestion.length) {
                        return rand;

                    } else if (counter === 0) {
                        return rand;
                    } else {
                        counter++;
                    }
                }

            } while (counter < usedQuestion.length)
        } else {
            console.log("za mało pytań")
        }

    }
    const quizQuestionList = (e) => {
        for (let i = 0; i < e; i++) {
            quizQuestion.push(Quest_temp[randQuestion(0, Quest_temp.length)])

        }
        return quizQuestion;
    }
    const handleCallback = (childData) => {

        setCategory(childData)
    }

    const playAgain = () => {
        quizQuestion = []
        quizQuestionList(numberOfQuestion)
        setPlay(false);
    }
    const rereollquestion = (playA) => {
        setPlay(playA)
        if (play){
            playAgain()
        }

    }


    return (
        <div>
            <div>
                {name}
            </div>
            <div>
                <QuestionBox questionList={quizQuestionList(numberOfQuestion)} setTest={handleCallback} rerolquestion={rereollquestion}/>
            </div>
        </div>
    )
}
export default QuestionLoader;