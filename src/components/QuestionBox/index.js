import React, {useState} from 'react'
import ScorePage from "../../pages/ScorePage";
import QuestionBoxDnd from "../QuestionBoxDnd"
import QuestionBoxDndList from "../QuestionBoxDndList"
import {DndProvider} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import {TouchBackend} from 'react-dnd-touch-backend'

const QuestionBox = ({questionList, setTest, rerolquestion}) => {

    const [score, setScore] = useState(0)
    const [current, setNext] = useState(0)
    const [end, setEnd] = useState(false);
    const [choice, setChoice] = useState(false);
    const [device, setDevice] = useState(false)
    const [dnd, setDnd] = useState({data: ""})


    window.addEventListener('touchstart', function () {
        setDevice(true);
    })
//-------------------------- question logic ---------------------------------
    const clearnext = (answer, index) => {
        setTimeout(function () {

            let state = answer
            if (current + 1 < questionList.length) {
                setNext(current + 1)
            } else {
                setEnd(true);
            }
            setChoice(false);
            let test = document.getElementById(index)
            {
                test != null ? test.classList.remove(state) : console.log("test")
            }

        }, 1000)
    }

    const correctAnswer = (txt, questionId, index) => {
        if (questionList[questionId].type === 'dnd' || questionList[questionId].type === 'dnd-list') {
            if (JSON.stringify(txt.data) === JSON.stringify(questionList[questionId].correct_answer)) {
                document.getElementById('show-answer').classList.add("correct")
                setScore(score + 1)
                setTimeout(function () {
                    document.getElementById('show-answer').classList.remove("correct")
                if (current + 1 < questionList.length) {
                    setNext(current + 1)
                } else {
                    setEnd(true);
                }
            },1000)} else {
                document.getElementById('show-answer').classList.add("incorrect")
                setTimeout(function () {
                    document.getElementById('show-answer').classList.remove("incorrect")
                if (current + 1 < questionList.length) {
                    setNext(current + 1)
                } else {
                    setEnd(true);
                }
                },1000)}
        } else {
            if (!choice) {
                if (txt === questionList[questionId].correct_answer) {
                    setScore(score + 1)
                    document.getElementById(index).classList.add("correct")
                    setChoice(true);
                    {
                        current < questionList.length ? clearnext("correct", index) : <p>zdobyłeś {score}</p>
                    }


                } else {

                    document.getElementById(index).classList.add("in-correct")
                    setChoice(true);
                    {
                        current < questionList.length ? clearnext("in-correct", index) : <p>zdobyłeś {score}</p>
                    }
                }
            }
        }
    }

    // ---------------play againd and new category--------------------------
    const playAgain = () => {
        rerolquestion(true);
        setScore(0);
        setNext(0);
        setEnd(false);
    }
    const newCategory = () => {
        setTest(false)

    }
    //----------------------check dnd section------------------------------
    const checkAnswer = () => {
        const id = 0;
        correctAnswer(document.getElementById("answer").innerHTML, current, id)

    }
    const checkAnswerList = (childData) => {
        const id = 0;
        setDnd(childData);
        correctAnswer(dnd,current,id)

    }
    //------------------Question box select -----------------------------
    const questionComponent_select = <div>
        {questionList[current].question}
        <div>
            {questionList[current].answers.map((text, index) =>
                (
                    <button key={index} id={index} onClick={() => {
                        correctAnswer(text, current, index)
                    }}
                            className={"answer-button"}

                    >{text}</button>
                )
            )}
        </div>
    </div>;
    //----------------------Question box dnd-------------------------



    const questionComponent_dnd = <div>
        <p>dnd box</p>

        {device ?
            <DndProvider backend={TouchBackend}>
                <QuestionBoxDnd props={questionList[current]} checkAnswer={checkAnswer}/>
            </DndProvider> :
            <DndProvider backend={HTML5Backend}>
                <QuestionBoxDnd props={questionList[current]} checkAnswer={checkAnswer}/>
            </DndProvider>
        }
    </div>;

    //----------------------Question box dnd-list----------------------
    const questionComponent_dnd_list = <div>
        <p>dnd list box</p>

        {device ?
            <DndProvider backend={TouchBackend}>
                <QuestionBoxDndList props={questionList[current]} checkAnswerList={checkAnswerList}/>
            </DndProvider> :
            <DndProvider backend={HTML5Backend}>
                <QuestionBoxDndList props={questionList[current]} checkAnswerList={checkAnswerList}/>
            </DndProvider>
        }


    </div>;
    //---------------------------------------------------------------

//TODO add feature for select new category after end game
    return (
        <div className="QuestionBox">

            {end ? (<ScorePage score={score}
                               num_question={questionList.length}
                               playAgain={playAgain}
                               newCategory={newCategory}/>)
                : questionList[current].type === "select" ?
                    questionComponent_select : questionList[current].type === "dnd" ? questionComponent_dnd : questionComponent_dnd_list}

        </div>


    )

}
export default QuestionBox;