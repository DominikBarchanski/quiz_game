import React, {useEffect, useState} from 'react';
import {DragDropContext,Droppable, Draggable, } from "react-beautiful-dnd";

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};
const Choice = ({choice, index}) => {
    return (
        <Draggable draggableId={'a' + index} index={index}>
            {provided => (
                <div className={"dnd_item"} ref={provided.innerRef}
                     {...provided.draggableProps}
                     {...provided.dragHandleProps}
                >
                    {choice}

                </div>
            )}
        </Draggable>
    );
};

const QuestionList = React.memo(function QuestionList({choices}) {

    return choices.map((choice, index) => (
        <Choice choice={choice} index={index} key={index} />
    ));
});

const QuestionBoxDndList = ({props,checkAnswerList}) => {
    const [question, setQuestion] = useState();
    const [choices, setChoices] = useState([]);
    const [userResult, setUserResult] = useState("");


    useEffect(() => {
        setQuestion(props.question);
        setChoices(props.answers)
    }, [props])

    const check = () =>  checkAnswerList({data: choices});

    const onDragEnd = result => {
        if (!result.destination) {
            return;
        }
        if (result.destination.index === result.source.index) {
            return;
        }
        const newChoices = reorder(choices,
            result.source.index,
            result.destination.index);

        setChoices(newChoices)
    }


    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <h3>
                {question}
            </h3>

            <div className={'choices_wrapper'}>
                <Droppable droppableId="list">


                    {provided => (
                        <div ref={provided.innerRef} {...provided.droppableProps}>
                            <QuestionList choices={choices}/>
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </div>
            <button onClick={()=>check()} id="show-answer">sprawdz</button>
        </DragDropContext>

    )
}
export default QuestionBoxDndList;